const QUANTITY = 10000
const STRING = 'test'
let arr = new Array(QUANTITY)
let obj = {}
let objArr = {}
let str = ''

for (let i = 0; i < QUANTITY; i++) {
  arr[i] = STRING
  obj[i.toString()] = STRING
  objArr[i] = STRING
}

for (let i = 0; i < 10; i++) {
  test()
}

function test () {
  str = ''
  console.time('array for')
  for (let i = 0; i < arr.length; i++) {
      str += arr[i]
  }
  console.timeEnd('array for')

  str = ''
  console.time('array for in')
  for (let i in arr) {
      str += arr[i]
  }
  console.timeEnd('array for in')

  str = ''
  console.time('object for')
  for (let i = 0; i < QUANTITY; i++) {
      str += obj[i.toString()]
  }
  console.timeEnd('object for')

  str = ''
  console.time('object for in')
  for (let i in obj) {
      str += obj[i.toString()]
  }
  console.timeEnd('object for in')

  str = ''
  console.time('object-array for')
  for (let i in objArr) {
      str += objArr[i]
  }
  console.timeEnd('object-array for')

  str = ''
  console.time('object-array for in')
  for (let i in objArr) {
      str += arr[i]
  }
  console.timeEnd('object-array for in')

  console.log('---')
}
