function observable (object) {
  for (let property in object) {
    let value = object[property]
    Object.defineProperty(object, property, {
      get: () => {
        console.log(`get property ${property}: ${value}`)
        return value
      },
      set: (value) => {
        console.log(`set property ${property}: ${value}`)
      }
    })
  }

  return object
}

var product = {
  price: 15000,
  size: 'L'
}

product = observable(product)
let size = product.size
product.price = 20000
