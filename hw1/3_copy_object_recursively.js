function copyObj (obj) {
  let copy = {}
  for (let prop in obj) {
    if (typeof obj[prop] === 'object') {
      copy = copyObj(obj[prop])
    }
    copy[prop] = obj[prop]
  }

  return copy
}

const product = {
  price: 15000,
  size: 'L'
}
const product2 = copyObj(product)
product2.price = 20000

console.log(product, product2)
